using UnityEngine;

public class CarCollision : MonoBehaviour
{
    public GameObject carGhost;
    private bool isColliding = false;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            carGhost.SetActive(false);
            isColliding = true;
            Debug.Log("He chocado");

            // Llamar al m�todo ReactivateGhost despu�s de 5 segundos
            Invoke("ReactivateGhost", 5f);
        }
    }


    private void ReactivateGhost()
    {
        if (isColliding)
        {
            carGhost.SetActive(true);
            Debug.Log("Aparezco");
        }
    }

}

