using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerCamera : MonoBehaviour
{
    public Transform player;
    public Vector3 offset;
    private Camera mainCamera;
    private float currentFOV = 60f;

    private void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        transform.position = player.position + offset;
        transform.rotation = player.rotation;

        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            if (currentFOV == 30f)
            {
                currentFOV = 60f;
            }
            else if (currentFOV == 60f)
            {
                currentFOV = 90f;
            }
            else if (currentFOV == 90f)
            {
                currentFOV = 30f;
            }

            mainCamera.fieldOfView = currentFOV;
        }
    }
}
