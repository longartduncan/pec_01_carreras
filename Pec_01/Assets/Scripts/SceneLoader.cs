using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public TimerController timerController;
    public GhostLapData bestLapSO;
    

    public void LoadMenuMain()
    {
        SceneManager.LoadScene("MenuMain");

    }

    public void ContinueLoadLevel1()
    {
        SceneManager.LoadScene("Level_1");
        
       
    }

    public void NewLoadLevel1()
    {
        SceneManager.LoadScene("Level_1");
        timerController.ResetBestTime();
        bestLapSO.Reset();
        

    }

    public void LoadSettings()
    {
        SceneManager.LoadScene("Settings");
    }

    public void LoadCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
