using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Video;

public class TimerController : MonoBehaviour
{
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI bestTimeText;
    public TextMeshProUGUI nowTimeText;
    public GameObject wayPointGoal;
    public GameObject finishUI;
    public GameObject videoClip;
    public GameObject exitScene;
    public GameObject goText;
    public GameObject CarController;

    private int wayPointCount = 0;
    private float bestTime = Mathf.Infinity; // Inicializar con un valor muy grande
    private bool timerStopped = false;
    private float elapsedTime = 0f;
    private float nowTime = 0f;

    void Start()
    {
        // Apagar UI
        videoClip.SetActive(false);
        finishUI.SetActive(false);
        exitScene.SetActive(false);

        // Cargar el mejor tiempo guardado
        if (PlayerPrefs.HasKey("BestTime"))
        {
            bestTime = PlayerPrefs.GetFloat("BestTime");
            bestTimeText.text = "Best Time: " + FormatTime(bestTime);
        }

        // Inicializar el tiempo actual en cero
        nowTimeText.text = "Now Time: " + FormatTime(nowTime);
    }

    void Update()
    {
        if (!timerStopped)
        {
            elapsedTime += Time.deltaTime;
            nowTime += Time.deltaTime;

            int minutes = Mathf.FloorToInt(elapsedTime / 60f);
            int seconds = Mathf.FloorToInt(elapsedTime % 60f);

            string timerString = FormatTime(elapsedTime);
            timerText.text = timerString;

            // Actualizar el tiempo actual de la partida
            string nowTimeString = FormatTime(nowTime);
            nowTimeText.text = "Now Time: " + nowTimeString;
        }

        // Detectar si se ha presionado la tecla "Delete"
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            ResetBestTime();
           
        }
    }

    public void ResetBestTime()
    {
        PlayerPrefs.DeleteKey("BestTime");
        bestTime = Mathf.Infinity;
        bestTimeText.text = "Best Time: --:--"; // Se puede establecer cualquier valor predeterminado que se desee
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            wayPointCount++;

            if (wayPointCount >= 1) // Verificar si el coche ha pasado tres veces por el waypoint
            {
                //Debug.Log("He terminado el juego");
                timerStopped = true;
                CarController.GetComponent<SimpleCarController>().enabled = false; // Detener el control del coche
                goText.SetActive(false);
                finishUI.SetActive(true); // Mostrar la imagen de Finished
                videoClip.SetActive(true); // Activar la imagen de VideoClip
                exitScene.SetActive(true); // Activar boton de salida

                // Formatear el tiempo de la partida y mostrarlo
                string nowTimeString = FormatTime(nowTime);
                nowTimeText.text = "Now Time: " + nowTimeString;

                // Actualizar el mejor tiempo si se ha conseguido un nuevo r�cord
                if (elapsedTime < bestTime)
                {
                    bestTime = elapsedTime;
                    bestTimeText.text = "Best Time: " + FormatTime(bestTime);
                    PlayerPrefs.SetFloat("BestTime", bestTime);
                }
            }
        }
    }

    // Funci�n para formatear el tiempo en minutos y segundos
    string FormatTime(float time)
    {
        int minutes = Mathf.FloorToInt(time / 60f);
        int seconds = Mathf.FloorToInt(time % 60f);
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    
}


