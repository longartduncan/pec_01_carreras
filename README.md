# Pec_01_Carreras



#PEC 1. Un juego de carreras.

Botones de Menú
Botón Play para comenzar un nuevo juego sin record
Botón Continue para seguir el juego con el mejor record que se ha acumulado

Teclas
Con las teclas top se arranca el coche, con left y right se controla la dirección, con 0 se cambia de vista.


Objetivo del juego
En el juego, los jugadores compiten contra el reloj en una pista de carreras muy desafiante. La pista tiene diferentes obstáculos que la hacen extremadamente difícil, como curvas muy cerradas, cambios de elevación bruscos y una sección rocosa peligrosa. Además, en algunos puntos, los jugadores tendrán que pasar sobre lava del volcán, donde podrían caer y perder, lo que agrega aún más peligro y emoción al juego.
Es fundamental que los jugadores sean cuidadosos al conducir, ya que un solo error podría hacer que el coche se estrelle o caiga por un precipicio, lo que resultaría en una pérdida del juego.
El hecho de que el juego se base en el tiempo significa que la competición se enfoca en habilidades de velocidad, precisión y control en lugar de colisiones con otros jugadores. Los jugadores tendrán que dominar la pista para obtener el mejor tiempo posible. Además, el juego cuenta con la función de competir contra el fantasma del jugador anterior, lo que agrega un elemento motivador para mejorar el rendimiento.
Ademas, es importante encontrar el equilibrio adecuado entre el desafió y la recompensa para que los jugadores puedan experimentar el estado de flujo (flow), en el cual se encuentran completamente inmersos en el juego, mientras disfrutan de la emoción y la satisfacción de superar obstáculos desafiantes en la pista.   

Mecánicas del juego
El juego presenta mecánicas que permiten a los jugadores acelerar y moverse como si estuvieran conduciendo un coche real, se regulo los parámetros de las ruedas para que el coche no se deslizara tanto en las curvas. Además, ofrece tres vistas diferentes que permiten a los jugadores sentirse más cerca o más alejados del vehículo según su preferencia. Una de las características más interesantes es la toma de tiempo récord, lo que desafía a los jugadores a competir contra sí mismos para mejorar su tiempo anterior en la pista. Una vez que establecen un tiempo récord, pueden enfrentarse al coche fantasma, que representa su tiempo anterior, y tratar de superarlo para establecer un nuevo récord.

Arte
El coche fue modelado con un diseño de los antiguos coches de la formula 1.

Codificación
Script SimpleCarController
Se encarga de manejar la dirección y velocidad del coche, así como también detectar si el coche está tocando el terreno.
Variables
List<AxleInfo> axleInfos: Una lista que contiene información sobre cada eje del coche, incluyendo las ruedas izquierda y derecha de cada eje y si están conectadas al motor o al sistema de dirección.
float maxMotorTorque: La cantidad máxima de torque que el motor del coche puede aplicar a las ruedas.
float maxSteeringAngle: El ángulo máximo de dirección que las ruedas pueden tener.
Collider terrainCollider: El collider asociado al terreno.
Métodos
FixedUpdate(): Un método que se encarga de actualizar la velocidad y dirección del coche. Por último, verifica si el coche está tocando el terreno y reduce la aceleración a la mitad si está en contacto con el terreno.
Clase AxleInfo:
WheelCollider leftWheel: La rueda izquierda del eje.
WheelCollider rightWheel: La rueda derecha del eje.
bool motor: Un booleano que indica si la rueda está conectada al motor o no.
bool steering: Un booleano que indica si la rueda puede girar para cambiar la dirección del coche o no.



Script TimerController
Se encarga de controlar el temporizador del juego, así como finalizar el juego, actualizandoy mostrando la información relevante al jugador.

Variables
timerText: representa el objeto de texto en el que se muestra el tiempo transcurrido.
bestTimeText: representa el objeto de texto en el que se muestra el mejor tiempo registrado.
wayPointGoal: representa el waypoint del objetivo que el jugador debe alcanzar para completar la carrera.
finishUI: representa la imagen que se muestra al jugador cuando este completa la carrera.
videoClip: representa el clip de video que se reproduce cuando el jugador completa la carrera.
CarController: representa el controlador del coche.
wayPointCount: cuenta el número de veces que el jugador ha pasado por el waypoint del objetivo.
bestTime: almacena el mejor tiempo registrado.
timerStopped: indica si el temporizador se ha detenido o no.
elapsedTime: almacena el tiempo transcurrido desde el inicio de la carrera.
Métodos
Start(): se encarga de desactivar la imagen de vídeo y el mensaje de finalización y cargar el mejor tiempo registrado.
Update(): se encarga de actualizar el tiempo transcurrido y mostrarlo en el objeto de texto correspondiente.
OnTriggerEnter(): Se encarga de comprobar si el jugador ha pasado por el waypoint del objetivo y detener el temporizador, desactivar el controlador del coche y mostrar la imagen de finalización y el video clip correspondientes.
FormatTime(): método que recibe un tiempo en segundos y lo formatea en formato minutos:segundos para su presentación en pantalla.

Script FollowPlayerCamera
Se encarga de controlador la cámara que sigue al jugador.
Variables
player: es una referencia al objeto Transform del jugador que se va a seguir.
Offset: indica la distancia entre la cámara y el jugador.
MainCamera: la cámara principal del juego.
CurrentFOV: es un valor que indica el campo de visión actual de la cámara.
Métodos
Start(): establece la referencia a la cámara principal.
Update(): ajusta la posición y rotación de la cámara para que siga al jugador. Además, permite al jugador cambiar el campo de visión de la cámara mediante la tecla "0".

Script CarCollision
Se encarga de adjuntar a un objeto para detectar colisiones entre el objeto y otros objetos y realizar ciertas acciones en consecuencia.
Variables
carGhost: guarda una referencia al GameObject del fantasma del coche.
isColliding: indica si el coche está colisionando con otro objeto.
Métodos
OnCollisionEnter(): si el objeto colisionado tiene el tag "Player", desactiva el objeto del fantasma del coche, establece isColliding a true y llama al método ReactivateGhost después de 5 segundo.
ReactivateGhost(): llama después de 5 segundos si el coche ha colisionado con el objeto con tag "Player". Este método reactiva el objeto del fantasma.

Script SceneLoader
Carga de escenas y salida del juego.
Métodos
public void LoadLevel1(): Este método carga la escena "Level_1".
public void LoadSettings(): Este método carga la escena "Settings".
public void LoadCredits(): Este método carga la escena "Credits".
public void ExitGame(): Este método cierra el juego.

Script GhostLapData
Ss un ScriptableObject que almacena información sobre la posición y rotación del coche del jugador en un punto determinado en el tiempo durante una carrera.
Variables
carPositions: una lista que almacena posiciones del coche.
carRotations: una lista que almacena rotaciones del coche.
Métodos
AddNewData(): añade nuevas posiciones y rotaciones a las listas carPositions y carRotations.
GetDataAt(): obtiene la posición y rotación en un determinado índice sample de las listas carPositions y carRotations, respectivamente.
Reset(): reinicia las listas carPositions y carRotations, eliminando todos los datos almacenados.

Script GhostManager
Este script se encarga de gestionar el fantasma del jugador, el cual se crea a partir de los datos grabados durante una vuelta del jugador.
Variables
simpleCarControllerObj: contiene el script SimpleCarController que controla al coche del jugador.
timeBetweenSamples: tiempo entre cada muestra que se toma durante la grabación de datos para el fantasma.
bestLapSO: objeto Scriptable que contendrá los datos de la mejor vuelta realizada por el jugador.
carToRecord: contiene el coche que se grabará para crear los datos del fantasma.
carToPlay: contiene el coche que se moverá según los datos grabados del fantasma.

shouldRecord: indica si se está grabando o no.
totalRecordedTime: tiempo total grabado.
currenttimeBetweenSamples: tiempo transcurrido entre muestras grabadas.
shouldPlay: indica si se está reproduciendo o no.
totalPlayedTime: tiempo total de reproducción.
currenttimeBetweenPlaySamples: tiempo transcurrido entre muestras durante la reproducción.
currentSampleToPlay: número de muestra actual que se está reproduciendo.
lastSamplePosition: posición de la muestra anterior.
lastSampleRotation: rotación de la muestra anterior.
nextPosition: siguiente posición de la muestra.
nextRotation: siguiente rotación de la muestra.
timerController: objeto que controla el temporizador.
wayPointCount: contador de puntos de ruta.
Métodos
StartRecording(): inicia la grabación de datos para el fantasma.
StopRecording(): detiene la grabación de datos para el fantasma.
StartPlaying(): inicia la reproducción del fantasma.
StopPlaying(): detiene la reproducción del fantasma.
HandleTestActionInputs(): se encarga de manejar la entrada de acciones del usuario, como iniciar y detener la grabación y reproducción de los datos del fantasma por medio de teclas.
El método Update(): es donde se gestiona la grabación y reproducción de los datos del fantasma. Durante la grabación, se toman muestras a intervalos regulares y se almacenan en un objeto Scriptable. Durante la reproducción, se interpolan las posiciones y rotaciones entre muestras para crear una animación suave.


